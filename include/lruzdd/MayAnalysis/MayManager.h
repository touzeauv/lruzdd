#ifndef LRUZDD_MAY_MANAGER_H
#define LRUZDD_MAY_MANAGER_H

namespace lruzdd
{

template<typename MayDomain>
class MayManager
{
public:
	using Value = typename MayDomain::t;

	MayManager(MayDomain& domain,
	           const Value& value) :
	           m_domain(domain),
	           m_current(value)
	{
	}

	inline void update(const otawa::icache::Access& acc)
	{
		m_domain.update(acc, m_current);
	}

	inline void restart(const Value& value)
	{
		m_domain.copy(m_current, value);
	}

	inline Value current() const
	{
		return m_current;
	}

	inline bool alwaysMiss() const
	{
		return m_domain.isAlwaysMiss(m_current);
	}
private:
	MayDomain& m_domain;
	Value m_current;
};

} // namespace lruzdd

#endif // LRUZDD_MAY_MANAGER_H

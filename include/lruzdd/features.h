//FIXME: Change copyright

/*
 *	features of lruzdd
 *	Copyright (c) 2016, IRIT UPS.
 *
 *	This file is part of OTAWA
 *
 *	OTAWA is free software; you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License, or
 *	(at your option) any later version.
 *
 *	OTAWA is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with OTAWA; if not, write to the Free Software
 *	Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 *	02110-1301  USA
 */
#ifndef LRUZDD_FEATURES_H_
#define LRUZDD_FEATURES_H_

#include <unordered_map>

#include <elm/data/Array.h>

#include <otawa/hard/Cache.h>
#include <otawa/icache/features.h>
#include <otawa/icat3/features.h>
#include <otawa/proc/Feature.h>

//#include <lruzdd/CacheState.h>
//#include <lruzdd/ExactMust/ExactMustAbstractValue.h>
//#include <lruzdd/MayAnalysis/MayAbstractValue.h>

namespace lruzdd
{

template <class T>
class Container
{
public:
	inline Container(void) { }
	inline Container(const otawa::icat3::LBlockCollection& c): m_sets(c.sets())
	{
		for(int i = 0; i < m_sets.size(); ++i)
			m_sets[i] = elm::AllocArray<T>(c[i].size());
	}
	T& get(const otawa::icat3::LBlock* lb)
	{
		return m_sets[lb->set()][lb->index()];
	}
private:
	elm::AllocArray<elm::AllocArray<T> > m_sets;
};

//class ACSManager
//{
//public:
//	ACSManager(otawa::WorkSpace* ws);
//	void start(otawa::Block* b);
//	void update(const otawa::icache::Access& acc);
//	otawa::icat3::category_t classify(const otawa::icat3::LBlock* lb);
//	void print(const otawa::icat3::LBlock* lb, elm::io::Output& out);
//
//private:
//	void initialize(const otawa::Bag<otawa::icache::Access>&, otawa::Block* b);
//	std::unordered_map<const otawa::icat3::LBlock*, CacheState> m_map;
//	const otawa::icat3::LBlockCollection& m_coll;
//};

enum class HitCategory
{
	AH,
	NC
};

enum class MissCategory
{
	AM,
	NC
};

extern otawa::p::feature EXACT_MUST_ANALYSIS_FEATURE;
extern otawa::p::id<HitCategory> HIT_CATEGORY;
extern otawa::p::feature EXACT_MAY_ANALYSIS_FEATURE;
extern otawa::p::id<MissCategory> MISS_CATEGORY;
//extern otawa::p::id<Container<YoungerSetAntichain<AntichainType::MAY> > > EXACT_MAY_INIT;
//extern otawa::p::id<Container<YoungerSetAntichain<AntichainType::MAY> > > EXACT_MAY_IN;

} // namespace lruzdd

#endif /* LRUZDD_FEATURES_H_ */

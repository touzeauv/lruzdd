#ifndef LRUZDD_GENERATORS_H
#define LRUZDD_GENERATORS_H

#include <set>

#include <otawa/icat3/features.h>

namespace lruzdd
{

class LexicalOrder;

class Generator
{
public:
	friend class LexicalOrder;

	using Block = otawa::icat3::LBlock;
	using BlockSet = std::set<const Block*>;

	Generator(const Block* focus,
	          const unsigned int ways,
	          bool focusEvicted);

	void update(const Block* block);
	bool contains(const Generator& generator) const;

	inline bool isFocusedBlockEvicted() const
	{
		return _focusEvicted;
	}

	inline bool contains(const Block* block) const
	{
		return _focusEvicted ||
		       (_youngerBlocks.find(block) != _youngerBlocks.end());
	}


	inline bool operator==(const Generator& rhs) const
	{
		if(_focusEvicted)
			return rhs._focusEvicted;

		return ((!rhs._focusEvicted) && _youngerBlocks == rhs._youngerBlocks);
	}

	inline bool operator!=(const Generator& rhs) const
	{
		return !(*this == rhs);
	}

private:
	const Block* _focus;
	const unsigned int _ways;
	bool _focusEvicted;
	BlockSet _youngerBlocks;
};

class LexicalOrder
{
public:
	inline bool operator()(const Generator& lhs,
	                       const Generator& rhs) const
	{
		if(lhs.isFocusedBlockEvicted())
			return true;
		if(rhs.isFocusedBlockEvicted())
			return false;
		return std::lexicographical_compare(
		       lhs._youngerBlocks.begin(),
		       lhs._youngerBlocks.end(),
		       rhs._youngerBlocks.begin(),
		       rhs._youngerBlocks.end());
	}
};

} // namespace lruzdd

#endif // LRUZDD_GENERATORS_H

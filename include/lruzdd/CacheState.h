#ifndef LRUZDD_CACHESTATE_H
#define LRUZDD_CACHESTATE_H

#include <elm/io/Output.h>
#include <otawa/icat3/features.h>
#include <otawa/icache/features.h>

#include <lruzdd/ExactMust/ExactMustDomain.h>
#include <lruzdd/ExactMay/ExactMayDomain.h>

namespace lruzdd
{

class CacheState {
public:
	CacheState(const otawa::icat3::LBlock* focus,
	           const otawa::icat3::LBlockCollection& coll,
	           int set,
	           typename ExactMustDomain::t& initMustState,
	           typename ExactMayDomain::t& initMayState) :
		m_focus(focus),
		m_coll(coll),
		m_mustDom(focus, coll, set, nullptr),
		m_mayDom(focus, coll, set, nullptr),
		m_mustState(initMustState),
		m_mayState(initMayState)
	{ }

	void update(const otawa::icache::Access& acc)
	{
		m_mustDom.update(acc, m_mustState);
		m_mayDom.update(acc, m_mayState);
	}

	otawa::icat3::category_t classify()
	{
		bool ah = m_mustState.alwaysHit(m_coll.A(), m_focus);
		bool am = m_mayState.alwaysMiss(m_coll.A(), m_focus);

		ASSERT(!am || !ah || "Classification cannot be AH and AM for the same access");
		if(ah)
			return otawa::icat3::AH;
		else if(am)
			return otawa::icat3::AM;
		else
			return otawa::icat3::NC;
	}

private:
	const otawa::icat3::LBlock* m_focus;
	const otawa::icat3::LBlockCollection& m_coll;
	ExactMustDomain m_mustDom;
	ExactMayDomain m_mayDom;
	typename ExactMustDomain::t m_mustState;
	typename ExactMayDomain::t m_mayState;
};

} // namespace lruzdd

#endif // LRUZDD_CACHESTATE_H

#ifndef LRUZDD_MUST_MANAGER_H
#define LRUZDD_MUST_MANAGER_H

namespace lruzdd
{

template<typename MustDomain>
class MustManager
{
public:
	using Value = typename MustDomain::t;

	MustManager(MustDomain& domain,
	            const Value& value) :
	            m_domain(domain),
	            m_current(value)
	{
	}

	inline void update(const otawa::icache::Access& acc)
	{
		m_domain.update(acc, m_current);
	}

	inline void restart(const Value& value)
	{
		m_domain.copy(m_current, value);
	}

	inline Value current() const
	{
		return m_current;
	}

	inline bool alwaysHit() const
	{
		return m_domain.isAlwaysHit(m_current);
	}
private:
	MustDomain& m_domain;
	Value m_current;
};

} // namespace lruzdd

#endif // LRUZDD_MAY_MANAGER_H

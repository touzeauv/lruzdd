#include <lruzdd/features.h>

#include <otawa/proc/BBProcessor.h>
#include <otawa/icat3/features.h>
#include <otawa/icache/features.h>

#include <lruexact/features.h>

using namespace otawa;

namespace lruzdd
{

class ClassificationBuilder : public BBProcessor
{
public:
	static otawa::p::declare reg;

	ClassificationBuilder() : otawa::BBProcessor(reg)
	{
	}

protected:

	virtual void processBB(WorkSpace*, CFG*, Block* b) override
	{
		if(!b->isBasic())
			return;

		BasicBlock* bb = b->toBasic();

		for(Block::EdgeIter edgeIter(bb->ins()); edgeIter(); ++edgeIter) {
			Edge* e = *edgeIter;
			Bag<icache::Access>& bag = icache::ACCESSES(e).ref();
			processBag(bag);
		}

		Bag<icache::Access>& bag = icache::ACCESSES(bb).ref();
		processBag(bag);
	}

	void processBag(Bag<icache::Access>& bag)
	{
		for(int i = 0; i < bag.size(); ++i)
			processAccess(&bag[i]);
	}

	void processAccess(icache::Access* access)
	{
		HitCategory hCat = HIT_CATEGORY(access);
		MissCategory mCat = MISS_CATEGORY(access);

		ASSERT((hCat != HitCategory::AH || mCat != MissCategory::AM) && "Access cannot be AH and AM");

		if(hCat == HitCategory::AH)
			lruexact::EXACT_CATEGORY(access) = lruexact::ExactCategory::AH;
		else if(mCat == MissCategory::AM)
			lruexact::EXACT_CATEGORY(access) = lruexact::ExactCategory::AM;
		else
			lruexact::EXACT_CATEGORY(access) = lruexact::ExactCategory::DU;
	}
};

p::declare ClassificationBuilder::reg = p::init("lruzdd::ClassificationBuilder", Version(1, 0, 0))
	.require(icache::ACCESSES_FEATURE)
	.require(EXACT_MAY_ANALYSIS_FEATURE)
	.require(EXACT_MUST_ANALYSIS_FEATURE)
	.provide(lruexact::LRU_CLASSIFICATION_FEATURE)
	.make<ClassificationBuilder>();

} // namespace lruzdd


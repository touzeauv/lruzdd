#ifndef LRUZDD_ABSTRACT_VALUE_H
#define LRUZDD_ABSTRACT_VALUE_H

namespace lruzdd
{

template <typename AbstractValuePolicy>
class AbstractValue : public AbstractValuePolicy
{
public:
	friend AbstractValuePolicy;

	using Block = otawa::icat3::LBlock;

	AbstractValue();
	AbstractValue(const otawa::icat3::LBlock* focus, const otawa::icat3::LBlockCollection& coll, int set);
};

} // namespace lruzdd

#endif // LRUZDD_ABSTRACT_VALUE_H

#ifndef LRUZDD_DOMAIN_H_
#define LRUZDD_DOMAIN_H_

#include <otawa/icat3/features.h>
#include <otawa/icache/features.h>

#include "ZDD/ZDDAbstractValue.h"
#include "GeneratorsSet/GSAbstractValue.h"

namespace lruzdd
{

template<typename DomainPolicy>
class Domain : public DomainPolicy
{
public:
	using AbstractValue = typename DomainPolicy::AbstractValue;

	// This alias is needed by Otawa
	using t = AbstractValue;

	template <typename... Args>
	Domain(const otawa::icat3::LBlock* focus,
	       const otawa::icat3::LBlockCollection& coll,
	       int set,
	       const AbstractValue* init,
	       Args... args) :
		DomainPolicy(focus, coll, set, init, args...),
		m_focus(focus),
		m_bot(DomainPolicy::createBot(focus, coll, set)),
		m_top(DomainPolicy::createTop(focus, coll, set)),
		m_set(set),
		m_coll(coll),
		m_init(init ? *init : m_top),
		m_tmp(DomainPolicy::createTop(focus, coll, set))
	{
	}

	inline const AbstractValue& bot(void) const { return m_bot; }
	inline const AbstractValue& top(void) const { return m_top; }
	inline const AbstractValue& init(void) const { return m_init; }

	inline void copy(AbstractValue& d, const AbstractValue& s) { d = s; }
	inline bool equals(const AbstractValue& a, const AbstractValue& b) { return a == b; }
	inline void join(AbstractValue& d, const AbstractValue& s) {d.join(s);}

	void update(const otawa::icache::Access& access, AbstractValue& a)
	{
		switch(access.kind()) {

		case otawa::icache::FETCH:
			if(m_coll.cache()->set(access.address()) == m_set)
				fetch(a, otawa::icat3::LBLOCK(access));
			break;

		case otawa::icache::PREFETCH:
			if(m_coll.cache()->set(access.address()) == m_set) {
				copy(m_tmp, a);
				fetch(a, otawa::icat3::LBLOCK(access));
				join(a, m_tmp);
			}
			break;

		case otawa::icache::NONE:
			break;

		}
	}
private:
	void fetch(AbstractValue& a, const otawa::icat3::LBlock *lb)
	{
		DomainPolicy::fetch(a, lb, m_coll.A(), m_focus);
	}

	const otawa::icat3::LBlock* m_focus;
	AbstractValue m_bot, m_top;
	otawa::hard::Cache::set_t m_set;
	const otawa::icat3::LBlockCollection& m_coll;
	const AbstractValue& m_init;
	AbstractValue m_tmp;
};


} // namespace lruzdd

#endif /* LRUZDD_DOMAIN_H_ */


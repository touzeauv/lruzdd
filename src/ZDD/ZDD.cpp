#include "ZDD.h"

#include "ZDDManager.h"

extern "C"
{
#include <extra.h>

}

namespace lruzdd
{

ZDD ZDD::one(ManagerPtr manager)
{
	ASSERT(manager);
	return ZDD(manager, DD_ONE(manager->getManager()));
}
ZDD ZDD::zero(ManagerPtr manager)
{
	ASSERT(manager);
	return ZDD(manager, DD_ZERO(manager->getManager()));
}


ZDD::ZDD() : _manager(nullptr), _zdd(nullptr)
{
}

ZDD::ZDD(ManagerPtr manager, DdNode* zdd) : _manager(manager), _zdd(zdd)
{
	ASSERT(manager);
	ASSERT(zdd);

	Cudd_Ref(_zdd);
}

ZDD::ZDD(const ZDD& other) : _manager(other._manager), _zdd(other._zdd)
{
	if(_zdd != nullptr)
		Cudd_Ref(_zdd);
}

ZDD::~ZDD()
{
	if(_zdd != nullptr)
		Cudd_RecursiveDerefZdd(_manager->getManager(), _zdd);
}

ZDD& ZDD::operator=(const ZDD& other)
{
	if(_zdd != nullptr)
		Cudd_RecursiveDerefZdd(_manager->getManager(), _zdd);

	_zdd = other._zdd;
	if(_zdd != nullptr)
		Cudd_Ref(_zdd);

	_manager = other._manager;

	return *this;
}

bool ZDD::operator==(const ZDD& other) const
{
	return _zdd == other._zdd;
}

bool ZDD::operator!=(const ZDD& other) const
{
	return !(*this == other);
}


// TODO: Implement as "external" binary operator
ZDD ZDD::minUnion(const ZDD& other) const
{
	ASSERT(_manager != nullptr);
	ASSERT(other._manager != nullptr);
	ASSERT(_zdd != nullptr);
	ASSERT(other._zdd != nullptr);
	ASSERT(_manager == other._manager);

	// TODO: Check failure
	DdNode* u = Extra_zddMinUnion(_manager->getManager(), _zdd, other._zdd);
	return ZDD(_manager, u);
}

// TODO: Implement as "external" binary operator
ZDD ZDD::maxUnion(const ZDD& other) const
{
	ASSERT(_manager != nullptr);
	ASSERT(other._manager != nullptr);
	ASSERT(_zdd != nullptr);
	ASSERT(other._zdd != nullptr);
	ASSERT(_manager == other._manager);

	// TODO: Check failure
	DdNode* u = Extra_zddMaxUnion(_manager->getManager(), _zdd, other._zdd);
	return ZDD(_manager, u);
}

// TODO: Implement as "external" binary operator
ZDD ZDD::maxDotProduct(const ZDD& other) const
{
	ASSERT(_manager != nullptr);
	ASSERT(other._manager != nullptr);
	ASSERT(_zdd != nullptr);
	ASSERT(other._zdd != nullptr);
	ASSERT(_manager == other._manager);

	// TODO: Check failure
	DdNode* u = Extra_zddMaxDotProduct(_manager->getManager(), _zdd, other._zdd);
	return ZDD(_manager, u);
}

// TODO: Implement as "external" binary operator
ZDD ZDD::minDotProduct(const ZDD& other) const
{
	ASSERT(_manager != nullptr);
	ASSERT(other._manager != nullptr);
	ASSERT(_zdd != nullptr);
	ASSERT(other._zdd != nullptr);
	ASSERT(_manager == other._manager);

	// TODO: Reduce at each step (code Extra_zddMinDotProduct) 
	// TODO: Check failure
	DdNode* res = Extra_zddMinDotProduct(_manager->getManager(), _zdd, other._zdd);
//	DdNode* u = Extra_zddDotProduct(_manager->getManager(), _zdd, other._zdd);
//	Cudd_Ref(u);
//	DdNode* res = Extra_zddMinimal(_manager->getManager(), u);
//	Cudd_Deref(u);
	return ZDD(_manager, res);
}

// return true if zdd has changed (ie. if some set contains has cardinality at least k)
bool ZDD::boundCardinality(unsigned int k)
{
	ASSERT((_zdd == nullptr) == (_manager == nullptr));
	ASSERT(_manager != nullptr);
	ASSERT(_zdd != nullptr);

	DdManager* manager = _manager->getManager();

	// TODO: Check failure
	DdNode* t = _zdd;
	switch(k) {
		case 0:
			_zdd = CardinalityHelper<0>::truncate(manager, _zdd);
			break;
		case 1:
			_zdd = CardinalityHelper<1>::truncate(manager, _zdd);
			break;
		case 3:
			_zdd = CardinalityHelper<3>::truncate(manager, _zdd);
			break;
		case 7:
			_zdd = CardinalityHelper<7>::truncate(manager, _zdd);
			break;
		case 15:
			_zdd = CardinalityHelper<15>::truncate(manager, _zdd);
			break;
		case 31:
			_zdd = CardinalityHelper<31>::truncate(manager, _zdd);
			break;
		case 63:
			_zdd = CardinalityHelper<63>::truncate(manager, _zdd);
			break;
		default:
			ASSERT(false); // Associativity is to big or not a power of 2
	}
	Cudd_Ref(_zdd);
	bool same = t == _zdd;
	Cudd_RecursiveDerefZdd(_manager->getManager(), t);
	ASSERT((_zdd == nullptr) == (_manager == nullptr));
	return !same;
}

void ZDD::printDotToFile(char* filename, char* function) const
{
	ASSERT((_zdd == nullptr) == (_manager == nullptr));
	ASSERT(_manager != nullptr);
	ASSERT(_zdd != nullptr);
	_manager->printDotToFile(_zdd, filename, function);
	ASSERT((_zdd == nullptr) == (_manager == nullptr));
}


} // namespace lruzdd



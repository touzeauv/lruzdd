#ifndef LRUZDD_ZDD_ABSTRACT_VALUE_H
#define LRUZDD_ZDD_ABSTRACT_VALUE_H

#include <otawa/icat3/features.h>

#include "ZDD.h"
#include "ZDDManager.h"

namespace lruzdd
{

template <typename AbstractValuePolicy>
class ZDDAbstractValue : public AbstractValuePolicy
{
public:
	friend AbstractValuePolicy;

	using Block = otawa::icat3::LBlock;
	using ManagerPtr = std::shared_ptr<ZDDManager>;

	// Build bottom
	ZDDAbstractValue();
	// Build top
	ZDDAbstractValue(const otawa::icat3::LBlock* focus, const otawa::icat3::LBlockCollection& coll, int set);
	void join(const ZDDAbstractValue& other);
	void update(ManagerPtr manager, const Block* block, int k, const Block* focus);
	bool operator==(const ZDDAbstractValue& other) const;
	bool operator!=(const ZDDAbstractValue& other) const;

protected:
	bool _isBottom;
	bool _isTop;
	ZDD _zdd;
};

template <typename AbstractValuePolicy>
ZDDAbstractValue<AbstractValuePolicy>::ZDDAbstractValue() :
	AbstractValuePolicy(),
	_isBottom(true),
	_isTop(false),
	_zdd()
{
}

template <typename AbstractValuePolicy>
ZDDAbstractValue<AbstractValuePolicy>::ZDDAbstractValue(const otawa::icat3::LBlock*, const otawa::icat3::LBlockCollection&, int) :
	AbstractValuePolicy(),
	_isBottom(false),
	_isTop(true),
	_zdd()
{
}

template <typename AbstractValuePolicy>
void ZDDAbstractValue<AbstractValuePolicy>::join(const ZDDAbstractValue& other)
{
	if(other._isBottom)
		return;

	if(_isBottom) {
		_isBottom = other._isBottom;
		_isTop = other._isTop;
		_zdd = other._zdd;
		return;
	}

	if(_isTop || other._isTop) {
		_isTop = true;
		_zdd = ZDD();
		return;
	}

	AbstractValuePolicy::join(other._zdd);
}

template <typename AbstractValuePolicy>
void ZDDAbstractValue<AbstractValuePolicy>::update(ManagerPtr manager, const Block* block, int k, const Block* focus)
{
	ASSERT(manager);

	if(_isBottom)
		return;

	AbstractValuePolicy::update(manager, block, k, focus);
}

template <typename AbstractValuePolicy>
bool ZDDAbstractValue<AbstractValuePolicy>::operator==(const ZDDAbstractValue& other) const
{
	return (_isBottom == other._isBottom)
	    && (_isTop == other._isTop)
	    && (_zdd == other._zdd);
}

template <typename AbstractValuePolicy>
bool ZDDAbstractValue<AbstractValuePolicy>::operator!=(const ZDDAbstractValue& other) const
{
	return !(*this == other);
}


} // namespace lruzdd

#endif // LRUZDD_ZDD_ABSTRACT_VALUE_H

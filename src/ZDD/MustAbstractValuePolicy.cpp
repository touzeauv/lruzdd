#include "MustAbstractValuePolicy.h"
#include "ZDDAbstractValue.h"

namespace lruzdd
{

bool MustAbstractValuePolicy::isAlwaysHit(const ZDDMustDomainPolicy&) const
{
	const AbstractValue& self = static_cast<const AbstractValue&>(*this);
	return !self._isTop;
}

void MustAbstractValuePolicy::update(ManagerPtr manager, const Block* block, int k, const Block* focus)
{
	AbstractValue& self = static_cast<AbstractValue&>(*this);

	if(block == focus) {
		self._zdd = ZDD::one(manager);
		self._isBottom = false;
		self._isTop = false;
		return;
	}

	if(self._isTop)
		return;

	self._zdd = self._zdd.maxDotProduct(manager->getZDDBlock(block->index()));

	ZDD tmp(self._zdd);
	self._zdd.boundCardinality(k-1);
	if(self._zdd != tmp) {
		self._isTop = true;
		self._zdd = ZDD();
	}
}

void MustAbstractValuePolicy::join(const ZDD& other)
{
	AbstractValue& self = static_cast<AbstractValue&>(*this);
	self._zdd = self._zdd.maxUnion(other);
}

} // namespace lruzdd

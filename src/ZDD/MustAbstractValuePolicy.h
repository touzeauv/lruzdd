#ifndef LRUZDD_MUST_ABSTRACT_VALUE_POLICY_H
#define LRUZDD_MUST_ABSTRACT_VALUE_POLICY_H

#include <memory>

#include "ZDDManager.h"

namespace lruzdd
{

template <typename Policy>
class ZDDAbstractValue;

class ZDDMustDomainPolicy;

class MustAbstractValuePolicy
{
public:
	using Block = otawa::icat3::LBlock;
	using ManagerPtr = std::shared_ptr<ZDDManager>;
	using AbstractValue = ZDDAbstractValue<MustAbstractValuePolicy>;


	bool isAlwaysHit(const ZDDMustDomainPolicy&) const;
protected:
	void update(ManagerPtr manager, const Block* block, int k, const Block* focus);

	void join(const ZDD& other);
};

} // namespace lruzdd

#endif // LRUZDD_MUST_ABSTRACT_VALUE_POLICY_H

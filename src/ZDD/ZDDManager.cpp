#include "ZDDManager.h"

#include <string>
#include <cstring>
#include <cudd.h>
#include <cuddInt.h>

namespace lruzdd
{

ZDDManager::ZDDManager(const otawa::icat3::LBlockSet& blockSet) :
	_blockSet(blockSet),
	_manager(Cudd_Init(0, _blockSet.size(), CUDD_UNIQUE_SLOTS, CUDD_CACHE_SLOTS, 0))
{
}

DdManager* ZDDManager::getManager()
{
	return _manager;
}

ZDDManager::~ZDDManager()
{
	Cudd_Quit(_manager);
}

ZDD ZDDManager::getZDDBlock(int blockIndex)
{
	DdNode* zdd = cuddZddGetNode(_manager, blockIndex, DD_ONE(_manager), DD_ZERO(_manager));

	//TODO: Check failure
	return ZDD(shared_from_this(), zdd);
}

void ZDDManager::printDotToFile(const DdNode* node, char* filename, char* function) const
{
	char** iname = new char* [_blockSet.size()];
	char* oname[] = {function};
	for(int i = 0; i < _blockSet.size(); ++i){
		std::string s("block ");
		s.append(std::to_string(_blockSet[i]->address().offset()));
		iname[i] = new char[s.size() + 1];
		std::strncpy(iname[i], s.c_str(), s.size() + 1);
	}
	FILE *outfile;
	outfile = fopen(filename,"w"); // TODO: Check results

	Cudd_zddDumpDot(_manager, 1, const_cast<DdNode**>(&node), iname, oname, outfile);

	for(int i = 0; i < _blockSet.size(); ++i)
		delete[] iname[i];
	delete[] iname;
	fclose (outfile);
}

} // namespace lruzdd


#ifndef LRUZDD_ZDD_MANAGER_H
#define LRUZDD_ZDD_MANAGER_H

#include <otawa/icat3/features.h>

#include "ZDD.h"

struct DdManager;
typedef struct DdNode DdNode;

namespace lruzdd
{

class ZDDManager : public std::enable_shared_from_this<ZDDManager>
{
public:
	explicit ZDDManager(const otawa::icat3::LBlockSet& blockSet);
	DdManager* getManager();
	~ZDDManager();
	ZDD getZDDBlock(int blockIndex);
	void printDotToFile(const DdNode* node, char* filename, char* function) const;

	inline bool operator==(const ZDDManager& other) const
	{
		return _manager == other._manager;
	}
private:
	const otawa::icat3::LBlockSet& _blockSet;
	DdManager* _manager;
};

} //namespace lruzdd

#endif //LRUZDD_ZDD_MANAGER_H

#ifndef LRUZDD_MAY_ABSTRACT_VALUE_POLICY_H
#define LRUZDD_MAY_ABSTRACT_VALUE_POLICY_H

#include <memory>

#include "ZDDManager.h"

namespace lruzdd
{

template <typename Policy>
class ZDDAbstractValue;

class ZDDMayDomainPolicy;

class MayAbstractValuePolicy
{
public:
	using Block = otawa::icat3::LBlock;
	using ManagerPtr = std::shared_ptr<ZDDManager>;

	using AbstractValue = ZDDAbstractValue<MayAbstractValuePolicy>;

	bool isAlwaysMiss(const ZDDMayDomainPolicy& domain) const;
protected:
	void update(ManagerPtr manager, const Block* block, int k, const Block* focus);

	void join(const ZDD& other);
};

} // namespace lruzdd

#endif // LRUZDD_MAY_ABSTRACT_VALUE_POLICY_H

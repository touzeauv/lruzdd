#ifndef LRUZDD_ZDD_H
#define LRUZDD_ZDD_H

#include <elm/assert.h>

#include <memory>
#include <util.h>

extern "C"
{
#include <cudd.h>
#include <cuddInt.h>
}


namespace lruzdd
{

class ZDDManager;

template <unsigned int bound>
class CardinalityHelper
{
public:
	static DdNode* truncate(DdManager* manager, DdNode* zdd)
	{
		if(zdd == DD_ZERO(manager))
			return zdd;
		if(zdd == DD_ONE(manager))
			return zdd;

		DdNode* res = cuddCacheLookup1Zdd(manager, CardinalityHelper<bound>::truncate, zdd);
		if (res)
			return res;

		DdNode* q_t = CardinalityHelper<bound - 1>::truncate(manager, Cudd_T(zdd));
		Cudd_Ref(q_t);

		DdNode* q_e = CardinalityHelper<bound>::truncate(manager, Cudd_E(zdd));
		Cudd_Ref(q_e);

		DdNode* q = cuddZddGetNode(manager, zdd->index, q_t, q_e);

		Cudd_Deref(q_t);
		Cudd_Deref(q_e);

		return q;
	}
};

template <>
class CardinalityHelper<0>
{
public:
	static DdNode* truncate(DdManager* manager, DdNode* zdd)
	{
		if(zdd == DD_ZERO(manager))
			return zdd;
		if(zdd == DD_ONE(manager))
			return zdd;

		DdNode* res = cuddCacheLookup1Zdd(manager, CardinalityHelper<0>::truncate, zdd);
		if (res)
			return res;

		return truncate(manager, Cudd_E(zdd));
	}
};

class ZDD
{
public:
	using Block = int;
	using ManagerPtr = std::shared_ptr<ZDDManager>;

	static ZDD one(ManagerPtr manager);
	static ZDD zero(ManagerPtr manager);

	ZDD();
	ZDD(ManagerPtr ptr, DdNode* zdd);
	ZDD(const ZDD& other);
	~ZDD();

	ZDD& operator=(const ZDD& other);
	bool operator==(const ZDD& other) const;
	bool operator!=(const ZDD& other) const;

	ZDD minUnion(const ZDD& node) const;
	ZDD maxUnion(const ZDD& node) const;
	ZDD maxDotProduct(const ZDD& node) const;
	ZDD minDotProduct(const ZDD& node) const;
	bool boundCardinality(unsigned int k);
	void printDotToFile(char* filename, char* function) const;

	bool isNull() const {return _zdd == nullptr;}
	ManagerPtr getManager() const {return _manager;}

private:

	ManagerPtr _manager;
	DdNode* _zdd;
};

} // namespace lruzdd

#endif // LRUZDD_ZDD_H


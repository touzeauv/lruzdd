#ifndef LRUZDD_ZDD_MAY_DOMAIN_POLICY_H
#define LRUZDD_ZDD_MAY_DOMAIN_POLICY_H

#include "ZDDAbstractValue.h"
#include "MayAbstractValuePolicy.h"

namespace lruzdd
{

class ZDDMayDomainPolicy
{
public:
	using ManagerPtr = std::shared_ptr<ZDDManager>;
	using AbstractValue = ZDDAbstractValue<MayAbstractValuePolicy>;

	ZDDMayDomainPolicy(const otawa::icat3::LBlock*,
	                   const otawa::icat3::LBlockCollection& coll,
	                   int set,
	                   const AbstractValue*) :
		m_manager(new ZDDManager(coll[set]))
	{
	}

	ZDDMayDomainPolicy(const otawa::icat3::LBlock*,
	                   const otawa::icat3::LBlockCollection& coll,
	                   int set,
	                   const AbstractValue*,
	                   ManagerPtr manager) :
		m_manager(manager)
	{
	}

	AbstractValue createBot(const otawa::icat3::LBlock*,
	                        const otawa::icat3::LBlockCollection&,
	                        int)
	{
		return AbstractValue();
	}
	AbstractValue createTop(const otawa::icat3::LBlock* focus,
	                        const otawa::icat3::LBlockCollection& coll,
	                        int set)
	{
		return AbstractValue(focus, coll, set);
	}

	ManagerPtr getManager() const
	{
		return m_manager;
	}

	void fetch(AbstractValue& a, const otawa::icat3::LBlock *lb, int k, const otawa::icat3::LBlock* focus)
	{
		a.update(m_manager, lb, k, focus);
	}

	bool isAlwaysMiss(const AbstractValue& a) const
	{
		return a.isAlwaysMiss(*this);
	}
private:
	ManagerPtr m_manager;
};

} // namespace lruzdd

#endif // LRUZDD_ZDD_MAY_DOMAIN_POLICY_H

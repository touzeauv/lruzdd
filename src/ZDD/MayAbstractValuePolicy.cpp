#include "MayAbstractValuePolicy.h"
#include "ZDDAbstractValue.h"
#include "ZDDMayDomainPolicy.h"

namespace lruzdd
{

bool MayAbstractValuePolicy::isAlwaysMiss(const ZDDMayDomainPolicy& domain) const
{
	const AbstractValue& self = static_cast<const AbstractValue&>(*this);
	return (self._zdd == ZDD::zero(domain.getManager()));
}

void MayAbstractValuePolicy::update(ManagerPtr manager, const Block* block, int k, const Block* focus)
{
	AbstractValue& self = static_cast<AbstractValue&>(*this);

	if(block == focus) {
		self._zdd = ZDD();
		self._isBottom = false;
		self._isTop = true;
		return;
	}


	ZDD zddBlock = manager->getZDDBlock(block->index());
	if(self._isTop) {
		self._isTop = false;
		self._zdd = zddBlock;
		return;
	}

	self._zdd = self._zdd.minDotProduct(zddBlock);
	self._zdd.boundCardinality(k-1);
}

void MayAbstractValuePolicy::join(const ZDD& other)
{
	AbstractValue& self = static_cast<AbstractValue&>(*this);
	self._zdd = self._zdd.minUnion(other);
}

} // namespace lruzdd


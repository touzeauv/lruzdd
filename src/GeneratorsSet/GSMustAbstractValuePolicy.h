#ifndef LRUZDD_GS_MUST_ABSTRACT_VALUE_POLICY_H
#define LRUZDD_GS_MUST_ABSTRACT_VALUE_POLICY_H

#include "GSAbstractValue.h"

namespace lruzdd
{

class GSMustDomainPolicy;

class GSMustAbstractValuePolicy
{
public:
	using Self = GSAbstractValue<GSMustAbstractValuePolicy>;
	class Comparator
	{
	public:
		inline bool operator()(const Generator& lhs, const Generator& rhs) const
		{
			return lhs.contains(rhs);
		}

	};

	bool isAlwaysHit(const GSMustDomainPolicy&) const;
};

} // namespace lruzdd

#endif // LRUZDD_GS_MUST_ABSTRACT_VALUE_POLICY_H

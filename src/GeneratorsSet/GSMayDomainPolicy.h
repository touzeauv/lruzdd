#ifndef LRUZDD_GS_MAY_DOMAIN_POLICY_H
#define LRUZDD_GS_MAY_DOMAIN_POLICY_H

namespace lruzdd
{

class GSMayDomainPolicy
{
public:
	using AbstractValue = GSAbstractValue<GSMayAbstractValuePolicy>;

	GSMayDomainPolicy(const otawa::icat3::LBlock*,
	                  const otawa::icat3::LBlockCollection&,
	                  int,
	                  const AbstractValue*)
	{
	}

	AbstractValue createBot(const otawa::icat3::LBlock*,
	                        const otawa::icat3::LBlockCollection&,
	                        int)
	{
		return AbstractValue();
	}
	AbstractValue createTop(const otawa::icat3::LBlock* focus,
	                        const otawa::icat3::LBlockCollection& coll,
	                        int)
	{
		return AbstractValue({Generator(focus, coll.A(), false)});
	}

	void fetch(AbstractValue& a, const otawa::icat3::LBlock *lb, int, const otawa::icat3::LBlock*)
	{
		a.update(lb);
	}

	bool isAlwaysMiss(const AbstractValue& a) const
	{
		return a.isAlwaysMiss(*this);
	}
};

} // namespace lruzdd

#endif // LRUZDD_GS_MAY_DOMAIN_POLICY_H

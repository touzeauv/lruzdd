#ifndef LRUZDD_GS_MAY_ABSTRACT_VALUE_POLICY_H
#define LRUZDD_GS_MAY_ABSTRACT_VALUE_POLICY_H

#include "GSAbstractValue.h"

namespace lruzdd
{

class GSMayDomainPolicy;

class GSMayAbstractValuePolicy
{
public:
	using Self = GSAbstractValue<GSMayAbstractValuePolicy>;
	class Comparator
	{
	public:
		inline bool operator()(const Generator& lhs, const Generator& rhs) const
		{
			return rhs.contains(lhs);
		}

	};

	bool isAlwaysMiss(const GSMayDomainPolicy&) const;
};

} // namespace lruzdd

#endif // LRUZDD_GS_MAY_ABSTRACT_VALUE_POLICY_H

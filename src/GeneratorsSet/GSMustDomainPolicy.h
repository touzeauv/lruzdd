#ifndef LRUZDD_GS_MUST_DOMAIN_POLICY_H
#define LRUZDD_GS_MUST_DOMAIN_POLICY_H

namespace lruzdd
{

class GSMustDomainPolicy
{
public:
	using AbstractValue = GSAbstractValue<GSMustAbstractValuePolicy>;

	GSMustDomainPolicy(const otawa::icat3::LBlock*,
	                   const otawa::icat3::LBlockCollection&,
	                   int,
	                   const AbstractValue*)
	{
	}

	AbstractValue createBot(const otawa::icat3::LBlock*,
	                        const otawa::icat3::LBlockCollection&,
	                        int)
	{
		return AbstractValue();
	}
	AbstractValue createTop(const otawa::icat3::LBlock* focus,
	                        const otawa::icat3::LBlockCollection& coll,
	                        int)
	{
		return AbstractValue({Generator(focus, coll.A(), true)});
	}

	void fetch(AbstractValue& a, const otawa::icat3::LBlock *lb, int, const otawa::icat3::LBlock*)
	{
		a.update(lb);
	}

	bool isAlwaysHit(const AbstractValue& a) const
	{
		return a.isAlwaysHit(*this);
	}
};

} // namespace lruzdd

#endif // LRUZDD_GS_MUST_DOMAIN_POLICY_H

#include "GSMustAbstractValuePolicy.h"

#include "GSMustDomainPolicy.h"

namespace lruzdd
{

bool GSMustAbstractValuePolicy::isAlwaysHit(const GSMustDomainPolicy&) const
{
	const Self& self = static_cast<const Self&>(*this);
	for(const auto& g : self._generators) {
		if(g.isFocusedBlockEvicted())
			return false;
	}
	return true;
}

} // namespace lruzdd


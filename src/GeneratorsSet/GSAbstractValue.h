#ifndef LRUZDD_GS_ABSTRACT_VALUE_H
#define LRUZDD_GS_ABSTRACT_VALUE_H

#include <set>

#include <lruzdd/Generator.h>

namespace lruzdd
{

template <typename AbstractValuePolicy>
class GSAbstractValue : public AbstractValuePolicy
{
public:
	friend AbstractValuePolicy;

	using Block = Generator::Block;
	using GeneratorsSet = std::set<Generator, LexicalOrder>;
	using PartialOrder = typename AbstractValuePolicy::Comparator;

	GSAbstractValue(const GeneratorsSet& g = {});
	void update(const Block* block);
	void join(const GSAbstractValue& rhs);
	inline bool operator==(const GSAbstractValue& rhs) const;
	inline bool operator!=(const GSAbstractValue& rhs) const;

private:
	void insert(const Generator& generator);

	GeneratorsSet _generators;
	PartialOrder _less;
};


template <typename AbstractValuePolicy>
GSAbstractValue<AbstractValuePolicy>::GSAbstractValue(const GeneratorsSet& g) :
	AbstractValuePolicy(),
	_generators(g)
{
}

template <typename AbstractValuePolicy>
void GSAbstractValue<AbstractValuePolicy>::update(const Block* block)
{
	GeneratorsSet old(_generators);
	_generators.clear();
	for(const auto& g : old) {
		Generator newGenerator(g);
		newGenerator.update(block);
		insert(newGenerator);
	}
}

template <typename AbstractValuePolicy>
void GSAbstractValue<AbstractValuePolicy>::join(const GSAbstractValue& rhs)
{
	for(const auto& g : rhs._generators) {
		insert(g);
	}
}

template <typename AbstractValuePolicy>
inline bool GSAbstractValue<AbstractValuePolicy>::operator==(const GSAbstractValue& rhs) const
{
	return _generators == rhs._generators;
}

template <typename AbstractValuePolicy>
inline bool GSAbstractValue<AbstractValuePolicy>::operator!=(const GSAbstractValue& rhs) const
{
	return !(*this == rhs);
}

} // namespace lruzdd

#endif // LRUZDD_GS_ABSTRACT_VALUE_H


#include "GSMayAbstractValuePolicy.h"

#include "GSMayDomainPolicy.h"

namespace lruzdd
{

bool GSMayAbstractValuePolicy::isAlwaysMiss(const GSMayDomainPolicy&) const
{
	const Self& self = static_cast<const Self&>(*this);
	for(const auto& g : self._generators) {
		if(!g.isFocusedBlockEvicted())
			return false;
	}
	return true;
}

} // namespace lruzdd


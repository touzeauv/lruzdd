#include <otawa/proc/ProcessorPlugin.h>

namespace lruzdd {

using namespace elm;
using namespace otawa;

class Plugin: public ProcessorPlugin {
public:
	Plugin(void): ProcessorPlugin("lruzdd", Version(1, 0, 0), OTAWA_PROC_VERSION) { }
};

} // namespace lruzdd

lruzdd::Plugin lruzdd_plugin;
ELM_PLUGIN(lruzdd_plugin, OTAWA_PROC_HOOK)


set(CUDD_LIB_FILE ${CMAKE_CURRENT_BINARY_DIR}/cudd/lib/libcudd.a)

add_custom_command(
	OUTPUT ${CUDD_LIB_FILE}
	COMMAND rm -rf cudd
	COMMAND git clone https://github.com/kozross/cudd
	COMMAND cd cudd && autoreconf -i
	COMMAND cd cudd && ./configure CC=${CMAKE_C_COMPILER} CFLAGS=-fPIC --prefix=${CMAKE_CURRENT_BINARY_DIR}/cudd
	COMMAND make -C cudd
	COMMAND make -C cudd install
	WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/
)

add_custom_target(
	libcudd
	DEPENDS ${CUDD_LIB_FILE}
)

#set(HEADERS_FOLDER ${CMAKE_CURRENT_BINARY_DIR}/cudd-2.3.1/include)
SET(CUDD_INCLUDE_DIR
	"${CMAKE_CURRENT_BINARY_DIR}/cudd/cudd/"
	"${CMAKE_CURRENT_BINARY_DIR}/cudd/st/"
	"${CMAKE_CURRENT_BINARY_DIR}/cudd/epd/"
	"${CMAKE_CURRENT_BINARY_DIR}/cudd/util/"
	"${CMAKE_CURRENT_BINARY_DIR}/cudd/dddmp/"
	"${CMAKE_CURRENT_BINARY_DIR}/cudd/mtr/"
	"${CMAKE_CURRENT_BINARY_DIR}/cudd/nanotrav/"
	"${CMAKE_CURRENT_BINARY_DIR}/cudd/"
)

file(MAKE_DIRECTORY ${CUDD_INCLUDE_DIR})

add_library(cudd STATIC IMPORTED GLOBAL)

set_target_properties(cudd PROPERTIES
	IMPORTED_LOCATION ${CUDD_LIB_FILE}
	INTERFACE_INCLUDE_DIRECTORIES "${CUDD_INCLUDE_DIR}"
)


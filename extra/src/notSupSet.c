#include <extra.h>

/**Function********************************************************************

  Synopsis    [Computes subsets in X that do not contain any of the subsets of Y.]

  Description []

  SideEffects []

  SeeAlso     [Extra_zddSubSet, Extra_zddSupSet, Extra_zddNotSubSet]

******************************************************************************/
DdNode  *
Extra_zddNotSupSet(
  DdManager * dd,
  DdNode * X,
  DdNode * Y)
{
    DdNode  *res;
    int     autoDynZ;

    autoDynZ = dd->autoDynZ;
    dd->autoDynZ = 0;

    do {
    dd->reordered = 0;
    res = extraZddNotSupSet(dd, X, Y);
    } while (dd->reordered == 1);
    dd->autoDynZ = autoDynZ;
    return(res);

} /* end of Extra_zddNotSupSet */

/**Function********************************************************************

  Synopsis [Performs the recursive step of Extra_zddNotSupSet.]

  Description []

  SideEffects [None]

  SeeAlso     []

******************************************************************************/
DdNode* extraZddNotSupSet( DdManager *dd, DdNode *X, DdNode *Y )
{   
    DdNode *zRes;
    statLine(dd); 
    /* any comb is a superset of itself */
    if ( X == Y ) 
        return DD_ZERO(dd);
    /* no comb in X is superset of non-existing combs */
    if ( Y == DD_ZERO(dd) ) 
        return X;   
    /* any comb in X is the superset of the empty comb */
    if ( Extra_zddEmptyBelongs( dd, Y ) ) 
        return DD_ZERO(dd);
    /* if X is empty, the result is empty */
    if ( X == DD_ZERO(dd) ) 
        return DD_ZERO(dd);
    /* if X is the empty comb (and Y does not contain it!), return it */
    if ( X == DD_ONE(dd) ) 
        return DD_ONE(dd);

    /* check cache */
    zRes = cuddCacheLookup2Zdd(dd, extraZddNotSupSet, X, Y);
    if (zRes)
        return(zRes);
    else
    {
        DdNode *zRes0, *zRes1, *zTemp;
        int TopLevelX = dd->permZ[ X->index ];
        int TopLevelY = dd->permZ[ Y->index ];

        if ( TopLevelX < TopLevelY )
        {
            /* combinations of X without label that are supersets of combinations of Y */
            zRes0 = extraZddNotSupSet( dd, cuddE( X ), Y );
            if ( zRes0 == NULL )  
                return NULL;
            cuddRef( zRes0 );

            /* combinations of X with label that are supersets of combinations of Y */
            zRes1 = extraZddNotSupSet( dd, cuddT( X ), Y );
            if ( zRes1 == NULL )  
            {
                Cudd_RecursiveDerefZdd( dd, zRes0 );
                return NULL;
            }
            cuddRef( zRes1 );

            /* compose Res0 and Res1 with the given ZDD variable */
            zRes = cuddZddGetNode( dd, X->index, zRes1, zRes0 );
            if ( zRes == NULL )
            {
                Cudd_RecursiveDerefZdd( dd, zRes0 );
                Cudd_RecursiveDerefZdd( dd, zRes1 );
                return NULL;
            }
            cuddDeref( zRes0 );
            cuddDeref( zRes1 );
        }
        else if ( TopLevelX == TopLevelY )
        {
            /* combs of X without var that are not supersets of combs of Y without var */
            zRes0 = extraZddNotSupSet( dd, cuddE( X ), cuddE( Y ) );
            if ( zRes0 == NULL )  
                return NULL;
            cuddRef( zRes0 );

            /* merge combs of Y with and without var */
            zTemp = cuddZddUnion( dd, cuddE( Y ), cuddT( Y ) );
            if ( zTemp == NULL )
            {
                Cudd_RecursiveDerefZdd( dd, zRes0 );
                return NULL;
            }
            cuddRef( zTemp );

            /* combs of X with label that are supersets of combs in Temp */
            zRes1 = extraZddNotSupSet( dd, cuddT( X ), zTemp );
            if ( zRes1 == NULL )
            {
                Cudd_RecursiveDerefZdd( dd, zRes0 );
                Cudd_RecursiveDerefZdd( dd, zTemp );
                return NULL;
            }
            cuddRef( zRes1 );
            Cudd_RecursiveDerefZdd( dd, zTemp );

            /* compose Res0 and Res1 with the given ZDD variable */
            zRes = cuddZddGetNode( dd, X->index, zRes1, zRes0 );
            if ( zRes == NULL ) 
            {
                Cudd_RecursiveDerefZdd( dd, zRes0 );
                Cudd_RecursiveDerefZdd( dd, zRes1 );
                return NULL;
            }
            cuddDeref( zRes0 );
            cuddDeref( zRes1 );
        }
        else /* if ( TopLevelX > TopLevelY ) */
        {
            /* combs of X that are supersets of combs of Y without label */
            zRes = extraZddNotSupSet( dd, X, cuddE( Y ) );
            if ( zRes == NULL )  return NULL;
        }

        /* insert the result into cache */
        cuddCacheInsert2(dd, extraZddNotSupSet, X, Y, zRes);
        return zRes;
    }
} /* end of extraZddNotSupSet */


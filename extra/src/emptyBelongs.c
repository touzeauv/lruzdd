#include <extra.h>

int 
Extra_zddEmptyBelongs(
  DdManager *dd,
  DdNode* zS )
{
	while ( zS->index != CUDD_MAXINDEX )
		zS = cuddE( zS );
	return (int)( zS == DD_ONE(dd) );

} /* end of Extra_zddEmptyBelongs */



#include <extra.h>

/**Function********************************************************************

  Synopsis    [Computes subsets in X that are not contained in any of the subsets of Y.]

  Description []

  SideEffects []

  SeeAlso     [Extra_zddSubSet, Extra_zddSupSet, Extra_zddNotSupSet]

******************************************************************************/
DdNode  *
Extra_zddNotSubSet(
  DdManager * dd,
  DdNode * X,
  DdNode * Y)
{
    DdNode  *res;
    int     autoDynZ;

    autoDynZ = dd->autoDynZ;
    dd->autoDynZ = 0;

    do {
    dd->reordered = 0;
    res = extraZddNotSubSet(dd, X, Y);
    } while (dd->reordered == 1);
    dd->autoDynZ = autoDynZ;
    return(res);

} /* end of Extra_zddNotSubSet */

/**Function********************************************************************

  Synopsis [Performs the recursive step of Extra_zddNotSubSet.]

  Description []

  SideEffects [None]

  SeeAlso     []

******************************************************************************/
DdNode* extraZddNotSubSet( DdManager *dd, DdNode *X, DdNode *Y )
{   
    DdNode *zRes;
    statLine(dd); 
    /* any comb is a subset of itself */
    if ( X == Y ) 
        return DD_ZERO(dd);
    /* combs in X are notsubsets of non-existant combs in Y */
    if ( Y == DD_ZERO(dd) ) 
        return X;   
    /* only {()} is the subset of {()} */
    if ( Y == DD_ONE(dd) ) /* remove empty combination from X */
        return cuddZddDiff( dd, X, DD_ONE(dd) );
    /* if X is empty, the result is empty */
    if ( X == DD_ZERO(dd) ) 
        return DD_ZERO(dd);
    /* the empty comb is contained in all combs of Y */
    if ( X == DD_ONE(dd) ) 
        return DD_ZERO(dd);

    /* check cache */
    zRes = cuddCacheLookup2Zdd(dd, extraZddNotSubSet, X, Y);
    if (zRes)
        return(zRes);
    else
    {
        DdNode *zRes0, *zRes1, *zTemp;
        int TopLevelX = dd->permZ[ X->index ];
        int TopLevelY = dd->permZ[ Y->index ];

        if ( TopLevelX < TopLevelY )
        {
            /* compute combs of X without var that are notsubsets of combs with Y */
            zRes0 = extraZddNotSubSet( dd, cuddE( X ), Y );
            if ( zRes0 == NULL )  
                return NULL;
            cuddRef( zRes0 );

            /* combs of X with var cannot be subsets of combs without var in Y */
            zRes1 = cuddT( X );

            /* compose Res0 and Res1 with the given ZDD variable */
            zRes = cuddZddGetNode( dd, X->index, zRes1, zRes0 );
            if ( zRes == NULL ) 
            {
                Cudd_RecursiveDerefZdd( dd, zRes0 );
                return NULL;
            }
            cuddDeref( zRes0 );
        }
        else if ( TopLevelX == TopLevelY )
        {
            /* merge combs of Y with and without var */
            zTemp = cuddZddUnion( dd, cuddE( Y ), cuddT( Y ) );
            if ( zTemp == NULL )  
                return NULL;
            cuddRef( zTemp );

            /* compute combs of X without var that are notsubsets of combs is Temp */
            zRes0 = extraZddNotSubSet( dd, cuddE( X ), zTemp );
            if ( zRes0 == NULL )  
            {
                Cudd_RecursiveDerefZdd( dd, zTemp );
                return NULL;
            }
            cuddRef( zRes0 );
            Cudd_RecursiveDerefZdd( dd, zTemp );

            /* combs of X with var that are notsubsets of combs in Y with var */
            zRes1 = extraZddNotSubSet( dd, cuddT( X ), cuddT( Y ) );
            if ( zRes1 == NULL )  
            {
                Cudd_RecursiveDerefZdd( dd, zRes0 );
                return NULL;
            }
            cuddRef( zRes1 );

            /* compose Res0 and Res1 with the given ZDD variable */
            zRes = cuddZddGetNode( dd, X->index, zRes1, zRes0 );
            if ( zRes == NULL ) 
            {
                Cudd_RecursiveDerefZdd( dd, zRes0 );
                Cudd_RecursiveDerefZdd( dd, zRes1 );
                return NULL;
            }
            cuddDeref( zRes0 );
            cuddDeref( zRes1 );
        }
        else /* if ( TopLevelX > TopLevelY ) */
        {
            /* merge combs of Y with and without var */
            zTemp = cuddZddUnion( dd, cuddE( Y ), cuddT( Y ) );
            if ( zTemp == NULL )  
                return NULL;
            cuddRef( zTemp );

            /* compute combs that are notsubsets of Temp */
            zRes = extraZddNotSubSet( dd, X, zTemp );
            if ( zRes == NULL ) 
            {
                Cudd_RecursiveDerefZdd( dd, zTemp );
                return NULL;
            }
            cuddRef( zRes );
            Cudd_RecursiveDerefZdd( dd, zTemp );
            cuddDeref( zRes );
        }

        /* insert the result into cache */
        cuddCacheInsert2(dd, extraZddNotSubSet, X, Y, zRes);
        return zRes;
    }
} /* end of extraZddNotSubSet */


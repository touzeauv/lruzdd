#include <extra.h>

/**Function********************************************************************

  Synopsis    [Computes the minimal of the union of two sets represented by ZDDs.]

  Description [This procedure assumes that the arguments are already
  minimal.]

  SideEffects []

  SeeAlso     [Extra_zddMaximal Extra_zddMimimal Extra_zddMaxUnion]

******************************************************************************/
DdNode  *
Extra_zddMinUnion(
  DdManager * dd,
  DdNode * S,
  DdNode * T)
{
    DdNode  *res;
    do {
    dd->reordered = 0;
    res = extraZddMinUnion(dd, S, T);
    } while (dd->reordered == 1);
    return(res);

} /* end of Extra_zddMinUnion */

/**Function********************************************************************

  Synopsis [Performs the recursive step of Extra_zddMinUnion.]

  Description []

  SideEffects [None]

  SeeAlso     []

******************************************************************************/
DdNode  *
extraZddMinUnion(
  DdManager * dd,
  DdNode * S,
  DdNode * T)
{
    DdNode *zRes;
    int TopS, TopT;
    statLine(dd); 

    /* consider terminal cases */
    if ( S == DD_ZERO(dd) )
        return T;
    if ( T == DD_ZERO(dd) )
        return S;
    if ( S == T )
        return S;
    /* the empty combination, if present, is the only minimal combination */
    if ( Extra_zddEmptyBelongs(dd, S) || Extra_zddEmptyBelongs(dd, T) )
        return DD_ONE(dd); 

    /* the operation is commutative - normalize the problem */
    TopS = dd->permZ[S->index];
    TopT = dd->permZ[T->index];

    if ( TopS > TopT || (TopS == TopT && (unsigned)S > (unsigned)T) )
        return extraZddMinUnion(dd, T, S);

    /* check cache */
    zRes = cuddCacheLookup2Zdd(dd, extraZddMinUnion, S, T);
    if (zRes)
        return(zRes);
    else
    {
        DdNode *zSet0, *zSet1, *zRes0, *zRes1;
        if ( TopS == TopT )
        {
            /* compute maximal for subsets without the top-most element */
            zSet0 = extraZddMinUnion(dd, cuddE(S), cuddE(T) );
            if ( zSet0 == NULL )
                return NULL;
            cuddRef( zSet0 );

            /* compute maximal for subsets with the top-most element */
            zSet1 = extraZddMinUnion(dd, cuddT(S), cuddT(T) );
            if ( zSet1 == NULL )
            {
                Cudd_RecursiveDerefZdd(dd, zSet0);
                return NULL;
            }
            cuddRef( zSet1 );
        }
        else /* if ( TopS < TopT ) */
        {
            /* compute maximal for subsets without the top-most element */
            zSet0 = extraZddMinUnion(dd, cuddE(S), T );
            if ( zSet0 == NULL )
                return NULL;
            cuddRef( zSet0 );

            /* subset with this element is just the cofactor of S */
            zSet1 = cuddT(S);
            cuddRef( zSet1 );
        }

        /* subset without this element remains unchanged */
        zRes0 = zSet0;

        /* remove subsets with this element that contain subsets without this element */
        zRes1 = extraZddNotSupSet(dd, zSet1, zSet0);
        if ( zRes1 == NULL )
        {
            Cudd_RecursiveDerefZdd(dd, zSet0);
            Cudd_RecursiveDerefZdd(dd, zSet1);
            return NULL;
        }
        cuddRef( zRes1 );
        Cudd_RecursiveDerefZdd(dd, zSet1);

        /* create the new node */
        zRes = cuddZddGetNode( dd, S->index, zRes1, zRes0 );
        if ( zRes == NULL ) 
        {
            Cudd_RecursiveDerefZdd( dd, zRes0 );
            Cudd_RecursiveDerefZdd( dd, zRes1 );
            return NULL;
        }
        cuddDeref( zRes0 );
        cuddDeref( zRes1 );

        /* insert the result into cache */
        cuddCacheInsert2(dd, extraZddMinUnion, S, T, zRes);
        return zRes;
    }
} /* end of extraZddMinUnion */


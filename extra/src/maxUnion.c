#include <extra.h>

/**Function********************************************************************

  Synopsis    [Computes the maximal of the union of two sets represented by ZDDs.]

  Description [This procedure assumes that the arguments are already
  maximal. This is why Maximal(X) is not computed in the bottom cases 
  MaxUnion(X, {}) = MaxUnion({}, X) = Maximal(X), and X is returned.]

  SideEffects []

  SeeAlso     [Extra_zddMaximal Extra_zddMimimal Extra_zddMinUnion]

******************************************************************************/

DdNode  *
Extra_zddMaxUnion(
  DdManager * dd,
  DdNode * S,
  DdNode * T)
{
    DdNode  *res;
    do {
    dd->reordered = 0;
    res = extraZddMaxUnion(dd, S, T);
    } while (dd->reordered == 1);
    return(res);

} /* end of Extra_zddMaxUnion */


/**Function********************************************************************

  Synopsis [Performs the recursive step of Extra_zddMaxUnion.]

  Description []

  SideEffects [None]

  SeeAlso     []

******************************************************************************/
DdNode  *
extraZddMaxUnion(
  DdManager * dd,
  DdNode * S,
  DdNode * T)
{
    DdNode *zRes;
    int TopS, TopT;
    statLine(dd); 

    /* consider terminal cases */
    if ( S == DD_ZERO(dd) )
        return T;
    if ( T == DD_ZERO(dd) )
        return S;
    if ( S == T )
        return S;
    if ( S == DD_ONE(dd) )
        return T;
    if ( T == DD_ONE(dd) )
        return S;

    /* the operation is commutative - normalize the problem */
    TopS = dd->permZ[S->index];
    TopT = dd->permZ[T->index];

    if ( TopS > TopT || (TopS == TopT && (unsigned)S > (unsigned)T) )
        return extraZddMaxUnion(dd, T, S);

    /* check cache */
    zRes = cuddCacheLookup2Zdd(dd, extraZddMaxUnion, S, T);
    if (zRes)
        return zRes;
    else
    {
        DdNode *zSet0, *zSet1, *zRes0, *zRes1;

        if ( TopS == TopT )
        {
            /* compute maximal for subsets without the top-most element */
            zSet0 = extraZddMaxUnion(dd, cuddE(S), cuddE(T) );
            if ( zSet0 == NULL )
                return NULL;
            cuddRef( zSet0 );

            /* compute maximal for subsets with the top-most element */
            zSet1 = extraZddMaxUnion(dd, cuddT(S), cuddT(T) );
            if ( zSet1 == NULL )
            {
                Cudd_RecursiveDerefZdd(dd, zSet0);
                return NULL;
            }
            cuddRef( zSet1 );
        }
        else /* if ( TopS < TopT ) */
        {
            /* compute maximal for subsets without the top-most element */
            zSet0 = extraZddMaxUnion(dd, cuddE(S), T );
            if ( zSet0 == NULL )
                return NULL;
            cuddRef( zSet0 );

            /* subset with this element is just the cofactor of S */
            zSet1 = cuddT(S);
            cuddRef( zSet1 );
        }

        /* remove subsets without this element covered by subsets with this element */
        zRes0 = extraZddNotSubSet(dd, zSet0, zSet1);
        if ( zRes0 == NULL )
        {
            Cudd_RecursiveDerefZdd(dd, zSet0);
            Cudd_RecursiveDerefZdd(dd, zSet1);
            return NULL;
        }
        cuddRef( zRes0 );
        Cudd_RecursiveDerefZdd(dd, zSet0);

        /* subset with this element remains unchanged */
        zRes1 = zSet1;

        /* create the new node */
        zRes = cuddZddGetNode( dd, S->index, zRes1, zRes0 );
        if ( zRes == NULL ) 
        {
            Cudd_RecursiveDerefZdd( dd, zRes0 );
            Cudd_RecursiveDerefZdd( dd, zRes1 );
            return NULL;
        }
        cuddDeref( zRes0 );
        cuddDeref( zRes1 );

        /* insert the result into cache */
        cuddCacheInsert2(dd, extraZddMaxUnion, S, T, zRes);
        return zRes;
    }
} /* end of extraZddMaxUnion */


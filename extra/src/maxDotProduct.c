#include <extra.h>

/**Function********************************************************************

  Synopsis    [Computes the maximal of the DotProduct of the union of two sets represented by ZDDs.]

  Description [This procedure assumes that the arguments are already
  maximal.]

  SideEffects []

  SeeAlso     [Extra_zddDotProduct Extra_zddMaximal Extra_zddMinCrossProduct]

******************************************************************************/
DdNode  *
Extra_zddMaxDotProduct(
  DdManager * dd,
  DdNode * S,
  DdNode * T)
{
    DdNode  *res;
    do {
    dd->reordered = 0;
    res = extraZddMaxDotProduct(dd, S, T);
    } while (dd->reordered == 1);
    return(res);

} /* end of Extra_zddMaxDotProduct */

/**Function********************************************************************

  Synopsis [Performs the recursive step of Extra_zddMaxDotProduct.]

  Description []

  SideEffects [None]

  SeeAlso     []

******************************************************************************/
DdNode  *
extraZddMaxDotProduct(
  DdManager * dd,
  DdNode * S,
  DdNode * T)
{
    DdNode *zRes;
    int TopS, TopT;
    statLine(dd); 

    /* consider terminal cases */
    if ( S == DD_ZERO(dd) || T == DD_ZERO(dd) )
        return DD_ZERO(dd);
    if ( S == DD_ONE(dd) )
        return T;
    if ( T == DD_ONE(dd) )
        return S;

    /* the operation is commutative - normalize the problem */
    TopS = dd->permZ[S->index];
    TopT = dd->permZ[T->index];

    if ( TopS > TopT || (TopS == TopT && (unsigned)S > (unsigned)T) )
        return extraZddMaxDotProduct(dd, T, S);

    /* check cache */
    zRes = cuddCacheLookup2Zdd(dd, extraZddMaxDotProduct, S, T);
    if (zRes)
        return zRes;
    else
    {
        DdNode *zSet0, *zSet1, *zRes0, *zRes1, *zTemp;
        if ( TopS == TopT )
        {
            /* compute the union of two cofactors of T (T0+T1) */
            zTemp = extraZddMaxUnion(dd, cuddE(T), cuddT(T) );
            if ( zTemp == NULL )
                return NULL;
            cuddRef( zTemp );

            /* compute MaxDotProduct with the top element for subsets (S1, T0+T1) */
            zSet0 = extraZddMaxDotProduct(dd, cuddT(S), zTemp );
            if ( zSet0 == NULL )
            {
                Cudd_RecursiveDerefZdd(dd, zTemp);
                return NULL;
            }
            cuddRef( zSet0 );
            Cudd_RecursiveDerefZdd(dd, zTemp);

            /* compute MaxDotProduct with the top element for subsets (S0, T1) */
            zSet1 = extraZddMaxDotProduct(dd, cuddE(S), cuddT(T) );
            if ( zSet1 == NULL )
            {
                Cudd_RecursiveDerefZdd(dd, zSet0);
                return NULL;
            }
            cuddRef( zSet1 );

            /* compute the union of these two partial results (zSet0 + zSet1) */
            zRes1 = extraZddMaxUnion(dd, zSet0, zSet1 );
            if ( zRes1 == NULL )
            {
                Cudd_RecursiveDerefZdd(dd, zSet0);
                Cudd_RecursiveDerefZdd(dd, zSet1);
                return NULL;
            }
            cuddRef( zRes1 );
            Cudd_RecursiveDerefZdd(dd, zSet0);
            Cudd_RecursiveDerefZdd(dd, zSet1);

            /* compute MaxDotProduct for subsets without the top-most element */
            zRes0 = extraZddMaxDotProduct(dd, cuddE(S), cuddE(T) );
            if ( zRes0 == NULL )
            {
                Cudd_RecursiveDerefZdd(dd, zRes1);
                return NULL;
            }
            cuddRef( zRes0 );
        }
        else /* if ( TopS < TopT ) */
        {
            /* compute MaxDotProduct with the top element for subsets (S1, T) */
            zRes1 = extraZddMaxDotProduct(dd, cuddT(S), T );
            if ( zRes1 == NULL )
                return NULL;
            cuddRef( zRes1 );

            /* compute MaxDotProduct for subsets without the top-most element */
            zRes0 = extraZddMaxDotProduct(dd, cuddE(S), T );
            if ( zRes0 == NULL )
            {
                Cudd_RecursiveDerefZdd(dd, zRes1);
                return NULL;
            }
            cuddRef( zRes0 );
        }

        /* remove subsets without this element covered by subsets with this element */
        zRes0 = extraZddNotSubSet(dd, zTemp = zRes0, zRes1);
        if ( zRes0 == NULL )
        {
            Cudd_RecursiveDerefZdd(dd, zTemp);
            Cudd_RecursiveDerefZdd(dd, zRes1);
            return NULL;
        }
        cuddRef( zRes0 );
        Cudd_RecursiveDerefZdd(dd, zTemp);

        /* create the new node */
        zRes = cuddZddGetNode( dd, S->index, zRes1, zRes0 );
        if ( zRes == NULL ) 
        {
            Cudd_RecursiveDerefZdd( dd, zRes0 );
            Cudd_RecursiveDerefZdd( dd, zRes1 );
            return NULL;
        }
        cuddDeref( zRes0 );
        cuddDeref( zRes1 );

        /* insert the result into cache */
        cuddCacheInsert2(dd, extraZddMaxDotProduct, S, T, zRes);
        return zRes;
    }
} /* end of extraZddMaxDotProduct */


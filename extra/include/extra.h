#ifndef EXTRA_H
#define EXTRA_H

#include <stdio.h>
#include <cudd.h>
#include <cuddInt.h>

extern int         Extra_zddEmptyBelongs( DdManager *dd, DdNode* zS );

extern DdNode *    Extra_zddMaxUnion( DdManager *dd, DdNode * S, DdNode * T );
extern DdNode *     extraZddMaxUnion( DdManager *dd, DdNode * S, DdNode * T );
extern DdNode *    Extra_zddMinUnion( DdManager *dd, DdNode * S, DdNode * T );
extern DdNode *     extraZddMinUnion( DdManager *dd, DdNode * S, DdNode * T );

extern DdNode *    Extra_zddMaxDotProduct( DdManager *dd, DdNode * S, DdNode * T );
extern DdNode *     extraZddMaxDotProduct( DdManager *dd, DdNode * S, DdNode * T );
extern DdNode *    Extra_zddMinDotProduct( DdManager *dd, DdNode * S, DdNode * T );
extern DdNode *     extraZddMinDotProduct( DdManager *dd, DdNode * S, DdNode * T );

extern DdNode *    Extra_zddNotSubSet( DdManager *dd, DdNode * X, DdNode * Y );
extern DdNode *     extraZddNotSubSet( DdManager *dd, DdNode * X, DdNode * Y );
extern DdNode *    Extra_zddNotSupSet( DdManager *dd, DdNode * X, DdNode * Y );
extern DdNode *     extraZddNotSupSet( DdManager *dd, DdNode * X, DdNode * Y );

#endif // EXTRA_H

